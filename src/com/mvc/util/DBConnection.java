package com.mvc.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {

	public static Connection createConnection() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://cite1.nwmissouri.edu:3306/assessments";
        String user = "citeuser";
        String password = "citeuser";
        
        Connection con = DriverManager.getConnection(url, user, password);

		return con;
	}
}