package com.mvc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mvc.bean.LoginBean;
import com.mvc.util.DBConnection;

public class LoginDao {
	public String authenticateUser(LoginBean loginBean) throws ClassNotFoundException {

		String userName = loginBean.getUserName(); // Keeping user entered
													// values in temporary
													// variables.
		String password = loginBean.getPassword();

		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;

		String userNameDB = "";
		String passwordDB = "";

		try {
			// establishing connection
			con = DBConnection.createConnection();
			// Statement is used to write queries. Read more about it.
			statement = con.createStatement();

			// Here table name is users and userName,password are columns.
			// fetching all the records and storing in a resultset
			resultSet = statement
					.executeQuery("select userid,password from test");

			// until next row is present otherwise it returns false
			while (resultSet.next()) {
				// fetch the values present in database
				userNameDB = resultSet.getString("userid");
				passwordDB = resultSet.getString("password");

				if (userName.equals(userNameDB) && password.equals(passwordDB)) {
					return "SUCCESS"; // //If the user entered values are
										// already present in database, which
										// means user has already registered so
										// I will return SUCCESS message.
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Invalid user credentials"; // Just returning appropriate message
											// otherwise
	}
}