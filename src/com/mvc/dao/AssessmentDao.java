package com.mvc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mvc.util.DBConnection;

public class AssessmentDao {
	public static List<Object> getAssessmentsList() throws ClassNotFoundException {
		Connection con = null;
		Statement statement = null;
		ResultSet resultSet = null;
		List<Object> list = new ArrayList<>();
		try {

			con = DBConnection.createConnection();
			statement = con.createStatement();
			resultSet = statement.executeQuery("select assessments.assessmentName,sessions.sessionStart,assessments.assessmentDesc "
					+ "from assessments,sessions where assessments.assessmentID=sessions.assessmentID;");
			
			while (resultSet.next()) {
				
				System.out.println("Result set is:"+ resultSet);
				list.add(resultSet.getString("assessments.assessmentName"));
				list.add(resultSet.getDate("sessions.sessionStart"));
				list.add(resultSet.getString("assessments.assessmentDesc"));
				
			}
			System.out.println("Result set after while loop:"+ resultSet);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("List in DAO"+list);
		return list;
	}
}
