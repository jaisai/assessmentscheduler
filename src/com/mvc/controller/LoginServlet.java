package com.mvc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mvc.bean.HomeBean;
import com.mvc.bean.LoginBean;
import com.mvc.dao.AssessmentDao;
import com.mvc.dao.HomeDao;
import com.mvc.dao.LoginDao;

public class LoginServlet extends HttpServlet {

	public LoginServlet() {
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// Here username and password are the names which I have given in the
		// input box in Login.jsp page. Here I am retrieving the values entered
		// by the user and keeping in instance variables for further use.

		String userName = request.getParameter("username");
		String password = request.getParameter("password");

		// creating object for LoginBean class, which is a normal java class,
		// contains just getters and setters.
		// Bean classes are efficiently used in java to access user information
		// whenever required in the application
		LoginBean loginBean = new LoginBean();

		loginBean.setUserName(userName); // setting the username and password
											// through the loginBean object then
											// only you can get it in future.
		loginBean.setPassword(password);

		LoginDao loginDao = new LoginDao(); // creating object for LoginDao.
											// this class contains main logic of
											// the application.

		String userValidate;
		try {
			userValidate = loginDao.authenticateUser(loginBean);

			if (userValidate.equals("SUCCESS")) // If function returns success
			// string then user will be rooted
			// to Home page
			{
				request.setAttribute("userName", userName); // with
															// setAttribute()
				// you can define a
				// "key" and value pair
				// so that you can get
				// it in future using
				// getAttribute("key")

				List<Object> list = new ArrayList<>();
				try {
					list = AssessmentDao.getAssessmentsList();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("Anjali in servlet: " + list);

				request.setAttribute("assessmentsList", list);
				request.getRequestDispatcher("/Home.jsp").forward(request,
						response);// RequestDispatcher is used to
				// send the control to the
				// invoked page.
			} else {
				// if authenticateUser() function returns other than SUCCESS
				// string
				// it will be sent to login page again. Here the error message
				// returned
				// from function has been stores in a errMessage key.
				request.setAttribute("errMessage", userValidate);
				request.getRequestDispatcher("/Login.jsp").forward(request,
						response);// forwarding the request
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Calling
			// authenticateUser
			// function

	}

}