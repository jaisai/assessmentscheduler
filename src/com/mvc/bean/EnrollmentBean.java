package com.mvc.bean;

import java.util.Date;

public class EnrollmentBean {
	private int enrollmentID;
	private Date dateRegistered;

	public int getEnrollmentID() {
		return enrollmentID;
	}

	public void setEnrollmentID(int enrollmentID) {
		this.enrollmentID = enrollmentID;
	}

	public Date getDateRegistered() {
		return dateRegistered;
	}

	public void setDateRegistered(Date dateRegistered) {
		this.dateRegistered = dateRegistered;
	}

	@Override
	public String toString() {
		return "EnrollmentBean [enrollmentID=" + enrollmentID
				+ ", dateRegistered=" + dateRegistered + "]";
	}

}
