package com.mvc.bean;

import java.sql.Date;

public class SessionBean {
	private int sessionID;
	private Date sessionStart;
	private Date sessionEnd;
	private Date dateModified;
	private Date dateCreated;
	private int numOfParticipants;

	public int getSessionID() {
		return sessionID;
	}

	public void setSessionID(int sessionID) {
		this.sessionID = sessionID;
	}

	public Date getSessionStart() {
		return sessionStart;
	}

	public void setSessionStart(Date sessionStart) {
		this.sessionStart = sessionStart;
	}

	public Date getSessionEnd() {
		return sessionEnd;
	}

	public void setSessionEnd(Date sessionEnd) {
		this.sessionEnd = sessionEnd;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public int getNumOfParticipants() {
		return numOfParticipants;
	}

	public void setNumOfParticipants(int numOfParticipants) {
		this.numOfParticipants = numOfParticipants;
	}

	@Override
	public String toString() {
		return "SessionBean [sessionID=" + sessionID + ", sessionStart="
				+ sessionStart + ", sessionEnd=" + sessionEnd
				+ ", dateModified=" + dateModified + ", dateCreated="
				+ dateCreated + ", numOfParticipants=" + numOfParticipants
				+ "]";
	}

}
