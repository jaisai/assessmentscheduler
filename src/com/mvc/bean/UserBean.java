package com.mvc.bean;

import java.util.Date;

public class UserBean {
	private String userID;
	private String lastName;
	private String firstName;
	private Date registrationDate;

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public String toString() {
		return "UserBean [userID=" + userID + ", lastName=" + lastName
				+ ", firstName=" + firstName + ", registrationDate="
				+ registrationDate + "]";
	}

}
