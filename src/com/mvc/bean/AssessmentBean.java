package com.mvc.bean;

import java.util.Date;

public class AssessmentBean {

	private int assessmentID;
	private String assessmentName;
	private String assessmentDesc;
	private Date dateCreated;
	private Date dateModified;

	public int getAssessmentID() {
		return assessmentID;
	}

	public void setAssessmentID(int assessmentID) {
		this.assessmentID = assessmentID;
	}

	public String getAssessmentName() {
		return assessmentName;
	}

	public void setAssessmentName(String assessmentName) {
		this.assessmentName = assessmentName;
	}

	public String getAssessmentDesc() {
		return assessmentDesc;
	}

	public void setAssessmentDesc(String assessmentDesc) {
		this.assessmentDesc = assessmentDesc;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	@Override
	public String toString() {
		return "AssessmentBean [assessmentID=" + assessmentID
				+ ", assessmentName=" + assessmentName + ", assessmentDesc="
				+ assessmentDesc + ", dateCreated=" + dateCreated
				+ ", dateModified=" + dateModified + "]";
	}

}
