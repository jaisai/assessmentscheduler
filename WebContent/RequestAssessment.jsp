<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="assessment.css">
<script src="MyJavaScript.js"></script>
</head>
<body>
	<%@include file="Header.html"%>
	<br>
	<br>
	<br>
	<br>
	<div class="container">
	<div class="row">
		<div id="LeftContentContainer" class="col-md-2 hidden-xs">
                    <div class="bg-main">
                        
                    </div>
                </div>
		<div id="MainContentContainer" class="col-md-8">
			<div class="bg-main">

				<div class="container col-md-12">
					<div class="panel panel-primary" id="RequestForm">
						<div class="panel-heading">
							<strong class="h2">Request a Assessment</strong> <br> <strong
								class="h3"> <span id="MainContent_FormNameLBL"></span></strong>
						</div>
						<div class="panel-body grid">
							<div class="form-group col-md-12">
								<div id="topicBox" class="form-group">
									<label for="requesterTextBox">Topic:</label> <input
										name="ctl00$MainContent$requesterTextBox" type="text"
										id="requesterTextBox" class="form-control"
										placeholder="Requested Topic">
								</div>
							</div>
							<div class="form-group col-md-12">
								<div id="requestPresentDescArea" class="form-group">
									<label for="descriptionTextArea">Description:</label>
									<textarea name="ctl00$MainContent$descriptionTextArea" rows="4"
										cols="50" id="descriptionTextArea" class="form-control"
										placeholder="Description" style="resize: none;"></textarea>
								</div>
							</div>
							<div class="form-group col-md-12">
								<div class="form-group">
									<div class="checkbox">
										<label> <input id="MainContent_presentCheckbox"
											type="checkbox" name="ctl00$MainContent$presentCheckbox"
											onclick="javascript:setTimeout('__doPostBack(\'ctl00$MainContent$presentCheckbox\',\'\')', 0)">

											I'd like to present the topic
										</label>
									</div>
								</div>
							</div>

							<div class="row-fluid">
								<div class="col-md-12 btn-toolbar">
									<br> <input type="submit"
										name="ctl00$MainContent$submitForm" value="Submit"
										id="submitForm" class="btn btn-success btn pull-right">
									<input type="submit" name="ctl00$MainContent$cancelButton"
										value="Cancel" id="cancelButton" class="btn pull-right">
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div id="RightContentContainer" class="col-md-2">
                    <div class="bg-main">
                        
                    </div>
                </div>
	</div>
</div>
</body>
</html>