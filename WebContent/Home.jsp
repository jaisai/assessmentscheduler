
<%@page language="java" import="java.util.*"%>
<html>
<head>
<title>Home Page</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="assessment.css">
<script src="MyJavaScript.js"></script>
</head>
<body>
<%@include  file="Header.html" %>
	<br>
	<br>
	<div class="col-md-8 col-md-offset-2">
		<div class="bg-main">
			<div class="page-header">
				<h1>Home</h1>
			</div>
			<div id="MainContent_registeredMeetingsContainer">
				<div class="panel panel-primary" id="registeredMeetingsPanel">
					<div class="panel-heading">
						<strong class="h3">Registered</strong>
					</div>
					<div></div>
					<div class="alert alert-info" role="alert">Nothing seems to
						be here. Register for a workshop and it will show up here.</div>
				</div>
			</div>
			<div id="UpcominMeetings">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<strong class="h3">Upcoming Assessments</strong>
					</div>

					<div>
						<table class="table table-hover" cellspacing="0"
							style="border-collapse: collapse;">
							<tr>
								<th class="bg-primary" scope="col" style="border-width: 0px;">Assessment
									Name</th>
								<th class="bg-primary" scope="col" style="border-width: 0px;">Date</th>
								<th class="bg-primary" scope="col" style="border-width: 0px;">Location</th>
								<th class="bg-primary" scope="col" style="border-width: 0px;">&nbsp;</th>
							</tr>
							<%
								Iterator itr;
								List data = (List) request.getAttribute("assessmentsList");
								for (itr = data.iterator(); itr.hasNext();) {
							%>
							<tr>
								<td><%=itr.next()%></td>
								<td><%=itr.next()%></td>
								<td><%=itr.next()%></td>
								<td><input type="submit"
									class="btn btn-primary pull-right" value="Details"
									style="margin-left: 5px;"> <input type="submit"
									class="btn btn-success pull-right" value="Register"></td>
							</tr>

							<%
								}
							%>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br>
	<div class="col-md-2">
		<div class="bg-main">
			<a id="RightContent_assessRequest"
				class="btn btn-primary pull-right" href="RequestAssessment.jsp">Assessment
				Request</a>
		</div>
	</div>
	<a href="CreateAssessment.jsp">create assessment</a>
</body>
</html>
