<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="assessment.css">
<script src="MyJavaScript.js"></script>
</head>
<body>
	<%@include file="Header.html"%>
	<br>
	<br>
	<br>
	<div class="col-md-8 col-md-offset-2" id="mainContainer">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h1>Administration</h1>
			</div>
			<div class="panel-body">
				<div>
					<ul class="nav nav-pills" role="tablist">
						<li role="presentation" class="active"><a href="##workshops"
							aria-controls="Assessments" role="tab" data-toggle="tab">Assessments</a></li>
						<li role="presentation"><a href="##requests"
							aria-controls="requests" role="tab" data-toggle="tab">Requests
								<span class="badge" id="numberOfRequests" runat="server"></span>
						</a></li>
						<li role="presentation"><a href="##settings"
							aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
						<li role="presentation"><a href="##moreSettings"
							aria-controls="moreSettings" role="tab" data-toggle="tab">More</a></li>
					</ul>
				</div>
			</div>

			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="assessments">
					<div class="row">
						<div class="col-md-8 col-md-offset-1">
							<textBox class="form-control" id="SearchTxt"
								placeholder="Search for a workshop name"></textBox>
						</div>
						<div class="col-md-3">
							<button id="SearchBTN" class="btn btn-primary"
								OnClick="SearchBTN_Click">Search</button>
							<button type="button" class="btn btn-success" onclick="openPage('CreateAssessment.jsp')">
								<span class="glyphicon glyphicon-plus"></span> Create
							</button>
						</div>
					</div>
					<br>
					<div class="row">

						<div class="col-md-4 col-md-offset-1">
							<label for="sortByDropDownList">Sort By:</label> <select
								id="MainContent_sortByDropDown" class="form-control">
								<option selected="selected" value="sortByDateModifiedDesc">Date
									Modified - Newest to Oldest</option>
								<option value="sortByDateModifiedAsc">Date Modified -
									Oldest to Newest</option>
								<option value="sortByDateCreatedDesc">Date Created -
									Newest to Oldest</option>
								<option value="sortByDateCreatedAsc">Date Created -
									Oldest to Newest</option>

							</select>
						</div>
						<div class="col-md-2">
							<label for="filterByCategoryName">Category:</label> <select
								id="MainContent_filterByCategoryName" class="form-control">
								<option selected="selected" value="0">All</option>
								<option value="11">Teaching and Learning</option>
								<option value="12">20 Minute Mentors</option>
								<option value="13">Student Support</option>
								<option value="14">Tenure &amp; Promotion</option>
							</select>
						</div>
						<div class="col-md-2">
							<label>Archived:</label> <select
								id="MainContent_isArchivedDropDownList" class="form-control">
								<option selected="selected" value="0">Not Archived</option>
								<option value="1">Archived</option>

							</select>
						</div>
					</div>
					<br>
					<div>
					<table class="table table-hover" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">
					<tbody>
					<tr>
					<th class="bg-primary" scope="col">Assessment Name</th>
					<th class="bg-primary" scope="col">Assessment Description</th>
					<th class="bg-primary" scope="col">&nbsp;</th>
					</tr>
					
					</tbody>
					</table>
					
					</div>
				</div>
			</div>

		</div>

	</div>
</body>
</html>